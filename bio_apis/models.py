from django.db import models
from django.utils.crypto import get_random_string
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager,User
from django.dispatch import receiver
from datetime import datetime,timedelta
import time
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
import jwt
from django.conf import settings
from rest_framework import authentication, exceptions
from .models import User
# Create your models here.

def UniqueId():
    from django.utils.crypto import get_random_string
    return ''.join(get_random_string(length=6))

PROVIDER_TYPE_CHOICES = (
      (1, 'Institution'),
      (2, 'Private Practice'),
      (3, 'Individual Provider'),
)    

### Admin Profile #####

class AdminManager(BaseUserManager):

    def create_superuser(self,name,email,password ):
        if not email:
            raise ValueError('email is required')
        email =self.normalize_email(email)
        user = self.model(email=email,name = name)
        user.set_password(password)
        user.is_superuser=True    
        user.is_staff = True
        user.save(using=self._db)
        return user

class Admin(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(db_index=True, unique=True)
    name = models.CharField(max_length=100)
    is_staff=models.BooleanField(default=False)
    is_active=models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True) 
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name',]
    
    objects = AdminManager()
    def __str__(self):
        return self.email

class UserManager(BaseUserManager):
 
    def create_user(self, email,name,provider_description,provider_type, password=None):
        if email is None:
            raise TypeError('Users must have an email address.')
        user = User(provider_type=provider_type, provider_description=provider_description, 
                          email=self.normalize_email(email),name=name)
        user.set_password(password)
        user.save()
        return user

class User(Admin, PermissionsMixin):
    provider_description = models.TextField(blank=True,null=True)
    user_id = models.CharField(max_length=50,default=UniqueId,unique=True)
    provider_type = models.CharField(max_length=50,null=True)
    is_verified=models.BooleanField(default=False)

    
 
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name','provider_type', 'email',]
 
    objects = UserManager()
 
    def __str__(self):
        return self.name        

@receiver(post_save,sender = 'bio_apis.User')
def token(sender,instance=None,created=False,**kwargs):
    if created:
        Token.objects.create(user=instance)  

class PatientProfile(models.Model):
    createdby = models.ForeignKey(User,on_delete = models.CASCADE,default=0)
    UniqueId = models.CharField(default=UniqueId,max_length=250,unique=True)
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    age  = models.IntegerField()
    race = models.CharField(max_length=250)
    gender = models.CharField(max_length=250)
    smoking = models.CharField(max_length=250)
    marital_status = models.CharField(max_length=250)
    insurance = models.CharField(max_length=250)
    zipcode = models.CharField(max_length=250)
    bmi = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    mrn = models.CharField(max_length=50,default=UniqueId,unique=True)
    modified_by = models.CharField(max_length=250)
    
    def __str__(self):
       return self.first_name


class PatientMedical(models.Model):
    createdby = models.ForeignKey(User,on_delete = models.CASCADE,default=0)
    patient_profile = models.ForeignKey(PatientProfile,on_delete = models.SET_NULL,null = True)    
    procedure = models.CharField(default=UniqueId,max_length=250,unique=True)
    comorbidities = models.CharField(max_length=250)
    asa_status = models.CharField(max_length=250)
    opioid_medications  = models.IntegerField()
    surgical_history = models.CharField(max_length=250)
    narcotics_use = models.CharField(max_length=250)
    opioid_allergies = models.CharField(max_length=250)
    diabetes = models.BooleanField(default=False)
    deconditioned = models.BooleanField(default=False)
    history_of_addiction = models.CharField(max_length=250)
    inpatient = models.BooleanField(default=False)
    statirs_at_home = models.BooleanField(default=False)
    dob = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
       return self.patient_profile


class PatientComplete(models.Model):
    createdby = models.ForeignKey(User,on_delete = models.CASCADE,default=0)
    UniqueId = models.CharField(default=UniqueId,max_length=250,unique=True)
    first_name = models.CharField(max_length=250,null=True)
    last_name = models.CharField(max_length=250,null=True)
    age  = models.IntegerField(default=0,null=True)
    race = models.CharField(max_length=250,null=True)
    gender = models.CharField(max_length=250,null=True)
    smoking = models.CharField(max_length=250,null=True)
    marital_status = models.CharField(max_length=250,null=True)
    insurance = models.CharField(max_length=250,null=True)
    zipcode = models.CharField(max_length=250,null=True)
    bmi = models.IntegerField(default=0,null=True)
    height = models.IntegerField(default=0,null=True)
    weight = models.IntegerField(default=0,null=True)
    mrn = models.CharField(max_length=50,unique=True)
    modified_by = models.CharField(max_length=250,null=True)    
    procedure = models.CharField(max_length=250,null=True)
    comorbidities = models.CharField(max_length=250,null=True)
    asa_status = models.CharField(max_length=250,null=True)
    opioid_medications  = models.CharField(max_length=250,null=True)
    surgical_history = models.CharField(max_length=250,null=True)
    narcotics_use = models.CharField(max_length=250,null=True)
    opioid_allergies = models.CharField(max_length=250,null=True)
    diabetes = models.BooleanField(default=False)
    deconditioned = models.BooleanField(default=False)
    history_of_addiction = models.CharField(max_length=250,null=True)
    inpatient = models.BooleanField(default=False)
    statirs_at_home = models.BooleanField(default=False)
    dob = models.DateField(default=False, blank=True)

    def __str__(self):
       return self.mrn