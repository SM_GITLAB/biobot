## user profile serializer

from rest_framework import serializers
from bio_apis import models



from rest_framework import serializers, exceptions
from bio_apis import models
from bio_apis.models import User,Token
from django.db.models import Q
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError


class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=1,
        style = {'input_type':'password'}  ,
        write_only=True
        
    )
    user_id = serializers.CharField(
        max_length=128,
        min_length=6,
        read_only=True
    )

    token = serializers.CharField(max_length=255, read_only=True)
    class Meta:
        model = models.User
        fields = ('id','email','name','password','user_id','provider_type','provider_description','token')
 
    def create(self, validated_data):
        return models.User.objects.create_user(**validated_data)




class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    user_id = serializers.CharField(max_length=255, read_only=True)
    provider_type = serializers.CharField(max_length=255, read_only=True)



    def validate(self, data):

        email = data.get('email', None)
        password = data.get('password', None)
 
        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password is not found.'
            )
        try:
            userObj = User.objects.get(email=user.email)
        except User.DoesNotExist:
            userObj = None 
 
        try:
            if userObj is None:
                userObj = Employee.objects.get(email=user.email,)
        except Employee.DoesNotExist:
            raise serializers.ValidationError(
                'User with given email and password does not exists'
            )        
        if not userObj.is_verified:
            raise serializers.ValidationError(
                'This user has been deactivated.'
            )   
      
        return {
            'user_id': userObj.id,
            'email': userObj.email,
            'provider_type': userObj.provider_type,
            'token': Token.objects.get(user=userObj).key
            
        }


class PatientProfileSerializer(serializers.ModelSerializer):

    createdby =serializers.IntegerField(source = 'createdby.id',read_only=True)
    # modified_by =serializers.CharField(source = 'createdby.name',read_only=True)

    mrn = serializers.CharField(label='Medical Record Number')
    class Meta:
        model = models.PatientProfile
        fields = ('id','createdby','mrn','UniqueId','first_name','last_name','age','race','gender','smoking','marital_status','insurance','zipcode','bmi','height','weight','modified_by')
        extra_kwargs={
            'UniqueId':{
                'read_only': True               
            },
            'mrn':{
                'read_only': True               
            },
            'patient_profile':{
                'read_only': True               
            }
        }

class PatientMedicalSerializer(serializers.ModelSerializer):
    patient_profile = PatientProfileSerializer(many=True)
    createdby =serializers.CharField(source = 'createdby.id',read_only=True)
    # patient_profile =serializers.CharField(source = 'patient_profile.id')

    # modified_by =serializers.CharField(source = 'createdby.name',read_only=True)

    dob = serializers.CharField(label='Date Of Birth',read_only=True)
    statirs_at_home = serializers.BooleanField(label='Stairs At Home')

    class Meta:
        model = models.PatientMedical
        fields = ('id','createdby','patient_profile','procedure','procedure','comorbidities','asa_status','opioid_medications','surgical_history','narcotics_use','opioid_allergies','diabetes','deconditioned','history_of_addiction','inpatient','statirs_at_home','dob')
        extra_kwargs={
            'dob':{
                'read_only': True               
            }
        }

class SearchSerializer(serializers.ModelSerializer):

    # createdby =serializers.IntegerField(source = 'createdby.id',read_only=True)
    # modified_by =serializers.CharField(source = 'createdby.name',read_only=True)

    mrn = serializers.CharField(label='Medical Record Number')
    class Meta:
        model = models.PatientComplete
        fields = ('id','createdby','mrn','UniqueId','first_name','last_name','age')
        extra_kwargs={
            'UniqueId':{
                'read_only': True               
            },
            'mrn':{
                'read_only': True               
            },
            'patient_profile':{
                'read_only': True               
            }
        }


class PatientCompleteSerializer(serializers.ModelSerializer):
    # patient_profile = PatientProfileSerializer(many=True)
    createdby =serializers.IntegerField(source = 'createdby.id',read_only=True)
    # patient_profile =serializers.CharField(source = 'patient_profile.id')

    # modified_by =serializers.CharField(source = 'createdby.name',read_only=True)

    dob = serializers.DateField(label='Date Of Birth',)
    statirs_at_home = serializers.BooleanField(label='Stairs At Home')

    class Meta:
        model = models.PatientComplete
        fields = "__all__"
        extra_kwargs={
            'dob':{
                'read_only': True               
            },
            'UniqueId':{
                'read_only': True               
            }
        }





