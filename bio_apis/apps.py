from django.apps import AppConfig


class BioApisConfig(AppConfig):
    name = 'bio_apis'
