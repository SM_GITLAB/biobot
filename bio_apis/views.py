from django.shortcuts import render
from bio_apis import serializers
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.shortcuts import render, redirect
from django.contrib import messages, auth
from rest_framework import viewsets
# from django.contrib.auth.models import User
from django.core.mail import send_mail, EmailMultiAlternatives
from django.conf import settings
from .tokens import account_activation_token
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import EmailMessage
from bio_apis.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.settings import api_settings
from rest_framework.renderers import JSONRenderer
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from bio_apis import permissions
from bio_apis import models
# Create your views here.


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_verified = True
        user.save()
        # login(request, user)
        # return redirect('home')
        # return render(request, 'bio_apis/login.html')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
        # return render(request, 'login.html', {'form': form})
    else:
        return HttpResponse('Activation link is invalid!')




class ProviderRegistration(APIView):
    # permission_classes = (AllowAny,)
    # renderer_classes = (JSONRenderer,)
    serializer_class = serializers.UserRegistrationSerializer
    queryset = User.objects.all()
    authentication_classes = (TokenAuthentication,)

 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)        
        serializer.is_active = False
        created_object = serializer.save()
        current_site = get_current_site(request)  
        current_site = get_current_site(request)
        html_content = render_to_string('emails/email_signup.html', {
        'user':created_object.name,
        'domain':current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(created_object.pk)),
        'token': account_activation_token.make_token(created_object),
        'mail': created_object.email
        })
        text_content = strip_tags(html_content)
        email = EmailMultiAlternatives(
            #subject
            'Verify your email',
            #content
            text_content,
            #from 
            'BioBot <noreply@thebiobot.com>',
            # to
            [created_object.email,]
        )
        email.attach_alternative(html_content,'text/html')
        email.send()     

        # admin Notification

        html_content_admin = render_to_string('emails/admin_user.html', {
        'user':created_object.name,
        'domain':current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(created_object.pk)),
        'token': account_activation_token.make_token(created_object),
        'provider_type': created_object.provider_type
        })
        text_content_admin = strip_tags(html_content_admin)
        email_admin = EmailMultiAlternatives(
            #subject
            'Verify your email',
            #content
            text_content_admin,
            #from 
            'BioBot <noreply@thebiobot.com>',
            # to
            ['samidalashankar@gmail.com',]
        )
        email_admin.attach_alternative(html_content_admin,'text/html')
        email_admin.send()
        messages.success(request,'Registered Scuccesfully')
        # return render(request, 'bio_apis/success.html') 
        return Response(serializer.data, status=status.HTTP_201_CREATED)


from django.contrib.auth import login as django_login ,logout as django_logout
from rest_framework.authtoken.models import Token

class UserLogin(APIView):
    # permission_classes = (AllowAny,)
    # renderer_classes = (JSONRenderer,)
    serializer_class = serializers.UserLoginSerializer
    authentication_classes = (TokenAuthentication,)
 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class UserLogout(APIView):
    authentication_classes = (TokenAuthentication,)

    def post(self, request):
        django_logout(request)
        return Response({'token':'exit'},status=204)


class PatientProfileviewset(viewsets.ModelViewSet):
    serializer_class=serializers.PatientProfileSerializer
    queryset = models.PatientProfile.objects.all()
    # model = models.PatientProfile    
    filter_backends = (filters.SearchFilter,)
    search_fields = ('=first_name','=last_name','=mrn')
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.UpdateOwnStatus,IsAuthenticated,permissions.IsUser,)


    def perform_create(self, serializer):
        serializer.save(createdby=self.request.user)



    # def perform_create(self, serializer):
    #     serializer.save(modified_by=self.request.user)    


""" autocomplete api """

# class MyModelList(APIView):
#     def get(self, request):
#                         #get all objects in model
#             modelList = models.PatientProfile.objects.all()
#             #convert to JSON                          #multiple objects in model
#             serializers = SearchSerializer(modelList, many=True)
#             #return the serialize JSON data
#             return Response(serializers.data)



class PatientMedicaleviewset(viewsets.ModelViewSet):
    serializer_class=serializers.PatientMedicalSerializer
    queryset = models.PatientMedical.objects.all()
    # model = models.PatientProfile
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name','last_name','mrn')
    authentication_classes = (TokenAuthentication,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.UpdateOwnStatus,IsAuthenticated)

    # def get_queryset(self):

    #     queryset = self.model.objects.all().filter(createdby=self.request.user)
    #     return queryset

    def perform_create(self, serializer):
        serializer.save(createdby=self.request.user)

    # def perform_create(self, serializer):
    #     serializer.save(modified_by=self.request.user)    

class Patientcompleteviewset(viewsets.ModelViewSet):
    serializer_class=serializers.PatientCompleteSerializer
    queryset = models.PatientComplete.objects.all()
    # model = models.PatientProfile
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name','last_name','mrn')
    authentication_classes = (TokenAuthentication,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.UpdateOwnStatus,IsAuthenticated,)

    # def get_queryset(self):

    #     queryset = self.model.objects.all().filter(createdby=self.request.user)
    #     return queryset

    def perform_create(self, serializer):
        serializer.save(createdby=self.request.user)

    # def perform_create(self, serializer):
    #     serializer.save(modified_by=self.request.user)    


class MyModelList(APIView):
    def get(self, request):
                        #get all objects in model
            modelList = models.PatientComplete.objects.all()
            #convert to JSON                          #multiple objects in model
            serializers = SearchSerializer(modelList, many=True)
            #return the serialize JSON data
            return Response(serializers.data)
