from django.urls import path, include
from rest_framework.routers import DefaultRouter
from bio_apis import views
from django.conf.urls import url



router= DefaultRouter()
# router.register('signup',views.Userviewset,base_name='Provider Registration')
router.register('patientprofile',views.PatientProfileviewset,base_name= 'PatientProfile')
router.register('patientmedical',views.PatientMedicaleviewset,base_name= 'PatientMedical')
router.register('patient',views.Patientcompleteviewset,base_name= 'Patientcomplete')



urlpatterns = [
    path('', include(router.urls)),
    path('list/', views.MyModelList.as_view(), name='list_view'),
    path('signup/',views.ProviderRegistration.as_view(),name='Sign UP'),
    path('login/',views.UserLogin.as_view(),name='Login'),
    path('logout/',views.UserLogout.as_view(),name='Logout'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),

]
